package com.quasar.fuego.operacion.exceptions;

public class SatelliteExceptions extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SatelliteExceptions(String message) {
		super(message);
	}
}
