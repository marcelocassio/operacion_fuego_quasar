package com.quasar.fuego.operacion.service.impl;

import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.springframework.stereotype.Service;

import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;
import com.quasar.fuego.operacion.service.LocalizacionService;

@Service
public class LocalizacionServiceImpl implements LocalizacionService {

	@Override
	public double[] getLocation(double[][] posicion, double[] distancia) {
		TrilaterationFunction trilaterationFunction = new TrilaterationFunction(posicion, distancia);
		NonLinearLeastSquaresSolver nonLinearLeastSquaresSolver = new NonLinearLeastSquaresSolver(trilaterationFunction,
				new LevenbergMarquardtOptimizer());
		return nonLinearLeastSquaresSolver.solve().getPoint().toArray();
	}

}
