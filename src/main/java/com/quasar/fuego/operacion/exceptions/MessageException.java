package com.quasar.fuego.operacion.exceptions;

public class MessageException extends Exception {

	/**
	 * Error handler to the Message Services
	 */
	private static final long serialVersionUID = 1L;

	public MessageException(String error) {
		super(error);
	}

}
