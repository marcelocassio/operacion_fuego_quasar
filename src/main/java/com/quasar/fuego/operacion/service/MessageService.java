package com.quasar.fuego.operacion.service;

import java.util.List;

import com.quasar.fuego.operacion.entities.Satellite;
import com.quasar.fuego.operacion.entities.SatelliteContainer;
import com.quasar.fuego.operacion.exceptions.MessageException;
import com.quasar.fuego.operacion.exceptions.SatelliteExceptions;

public interface MessageService {

	
	public String getMessage(List<List<String>> msgList) throws MessageException;
	public void saveMessages(Satellite satelliteData) throws SatelliteExceptions;
	public SatelliteContainer recoveryMessagesFromSatellites();
}
