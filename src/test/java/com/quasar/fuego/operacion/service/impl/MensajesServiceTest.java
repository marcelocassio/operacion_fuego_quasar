package com.quasar.fuego.operacion.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.quasar.fuego.operacion.exceptions.MessageException;
import com.quasar.fuego.operacion.service.MessageService;

@SpringBootTest
@RunWith(BlockJUnit4ClassRunner.class)
public class MensajesServiceTest {

	@Autowired
	MessageService messageService;

	@Test
	public void getMessage() throws MessageException {
		List<List<String>> messages = new ArrayList<List<String>>();
		String[] message1 = { "este", "", "", "mensaje" };
		String[] message2 = { "", "es", "", "" };
		String[] message3 = { "este", "", "un", "" };
		messages.add(Arrays.stream(message1).collect(Collectors.toList()));
		messages.add(Arrays.stream(message2).collect(Collectors.toList()));
		messages.add(Arrays.stream(message3).collect(Collectors.toList()));
		String message = messageService.getMessage(messages);
		String expectedMsg = "este es un mensaje";
		assertEquals(message, expectedMsg);
	}
	
	@Test
	public void getMessageWithBlank() throws MessageException {
		List<List<String>> messages = new ArrayList<List<String>>();
		String[] message1 = { "este", "", "", "mensaje" };
		String[] message2 = { "", "", "", "" };
		String[] message3 = { "este", "", "un", "" };
		messages.add(Arrays.stream(message1).collect(Collectors.toList()));
		messages.add(Arrays.stream(message2).collect(Collectors.toList()));
		messages.add(Arrays.stream(message3).collect(Collectors.toList()));
		String message = messageService.getMessage(messages);
		String expectedMsg = "este un mensaje";
		assertEquals(message, expectedMsg);
	}

	@Test
	public void getMessageWithPhaseShift() {
		List<List<String>> messages = new ArrayList<List<String>>();
		String[] message1 = { "este", "", "", "mensaje" };
		String[] message2 = { "", "es", "", "" };
		String[] message3 = { "este", "", "un", "", "" };
		messages.add(Arrays.stream(message1).collect(Collectors.toList()));
		messages.add(Arrays.stream(message2).collect(Collectors.toList()));
		messages.add(Arrays.stream(message3).collect(Collectors.toList()));
		try {
			String message = messageService.getMessage(messages);
		} catch (Exception e) {
			assertEquals("No se puede determinar el mensaje: Hay incongruencias en el tamaño del mensaje!",
					e.getMessage());
		}
	}
}
