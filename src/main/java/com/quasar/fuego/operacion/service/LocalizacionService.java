package com.quasar.fuego.operacion.service;

public interface LocalizacionService {

	public double[] getLocation(double[][] positions, double[] distances);

}
