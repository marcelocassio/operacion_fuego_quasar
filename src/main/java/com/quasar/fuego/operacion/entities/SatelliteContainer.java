package com.quasar.fuego.operacion.entities;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class SatelliteContainer {

	public SatelliteContainer() {
		satellites = new ArrayList<>();
	}

	private List<Satellite> satellites;
	int positionNumber = 2;

	public List<Satellite> getSatellites() {
		return satellites;
	}

	public void setSatellites(List<Satellite> satellites) {
		this.satellites = satellites;
	}

	public List<List<String>> getMessages() {
		List<List<String>> messages = new ArrayList<>();
		for (Satellite sat : satellites) {
			messages.add(sat.getMessage());
		}
		return messages;
	}

	public void setPositions(double[][] positions) {
		Position position;
		for (int i = 0; i < positions.length; i++) {
			position = new Position(positions[i]);
			satellites.get(i).setPosition(position);
		}
	}

	public double[][] getPositions() {
		double[][] positions = new double[satellites.size()][];
		double[] points = new double[positionNumber];

		for (int i = 0; i < satellites.size(); i++) {
			if (satellites.get(i).getPosition() != null) {
				points[0] = satellites.get(i).getPosition().getX();
				points[1] = satellites.get(i).getPosition().getY();
				positions[i] = points;
			}
		}
		return positions;
	}

	public double[] getDistances() {

		double[] distances = new double[satellites.size()];
		for (int i = 0; i < satellites.size(); i++) {
			distances[i] = satellites.get(i).getDistance();
		}
		return distances;
	}

}
