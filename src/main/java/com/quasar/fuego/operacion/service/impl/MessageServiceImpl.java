package com.quasar.fuego.operacion.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.quasar.fuego.operacion.dao.IMessageDao;
import com.quasar.fuego.operacion.entities.Satellite;
import com.quasar.fuego.operacion.entities.SatelliteContainer;
import com.quasar.fuego.operacion.exceptions.MessageException;
import com.quasar.fuego.operacion.exceptions.SatelliteExceptions;
import com.quasar.fuego.operacion.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
	private IMessageDao messageDao;

	@Autowired
	private Environment environment;

	/**
	 * Metodo principal, devuelve el mensaje si fue posible decodificarlo
	 */
	@Override
	public String getMessage(List<List<String>> messageList) throws MessageException {

		if (!verifyPhaseShift(messageList, getMessageSize(messageList)))
			throw new MessageException(
					"No se puede determinar el mensaje: Hay incongruencias en el tamaño del mensaje!");

		List<String> filteredMessage = filterMessage(messageList);
		return completeMessage(filteredMessage);
	}

	/**
	 * Este método transforma el mensaje dado de una lista a un String
	 * 
	 * @param msgList
	 * @return
	 */
	private String completeMessage(List<String> msgList) {
		return String.join(" ", msgList);
	}

	/**
	 * Este metodo filta todos los mensajes que vienen de los satelites
	 * 
	 * @param listaMensajes
	 * @return lista de palabras del mensaje
	 */
	private List<String> filterMessage(List<List<String>> messageList) {
		List<String> words = new ArrayList<>();

		for (int i = 0; i < getMessageSize(messageList); i++) {
			words.add("");
		}

		messageList.forEach(prhases -> {
			for (int i = 0; i < prhases.size(); i++) {
				if (!words.get(i).equals(prhases.get(i)) && !prhases.get(i).equals(""))
					words.set(i, prhases.get(i));
			}
		});

		return words;
	}

	private int getMessageSize(List<List<String>> messageList) {
		int messageSize = 0;

		for (List<String> prhases : messageList) {
			if (prhases.size() > messageSize) {
				messageSize = prhases.size();
			}
		}

		return messageSize;
	}

	/**
	 * Este metodo verifica si el tamaño del mensaje es correcto - Si la cantidad de
	 * mensajes son distintas no se podra evaluar el mensaje - Corresponde al
	 * defasaje
	 * 
	 * @param listAllMessages
	 * @param size
	 * @return true si es correcto, false si no lo es
	 */
	private boolean verifyPhaseShift(List<List<String>> listAllMessages, int size) {
		for (List<String> satMessage : listAllMessages) {
			if (satMessage.size() < size) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Este método emula una llamada a la base de datos. En este caso guardo la
	 * informacion en un objeto SatelliteContainer que voy a usar para devolver una
	 * respuesta
	 * 
	 * @throws SatelliteExceptions
	 */
	@Override
	public void saveMessages(Satellite satelliteData) throws SatelliteExceptions {
		int numberSatelities = Integer.parseInt(environment.getProperty("satellites.numbers"));
		SatelliteContainer container = messageDao.recoveryMessagesFromSatellites();
		if (container.getSatellites().size() < numberSatelities) {
			for (Satellite sat : container.getSatellites()) {
				if (sat.getName().equals(satelliteData.getName())) {
					throw new SatelliteExceptions("El satelite ya existe en la lista.");
				}
			}

		} else {
			throw new SatelliteExceptions("La cantidad de mensajes es inconsistente con la cantidad de satelites activos.");
		}
		messageDao.saveMessages(satelliteData);

	}

	/**
	 * Este metodo emula la llamada a una base de datos para recuperar la
	 * informacion de los satelites
	 */
	@Override
	public SatelliteContainer recoveryMessagesFromSatellites() {
		return messageDao.recoveryMessagesFromSatellites();
	}

}
