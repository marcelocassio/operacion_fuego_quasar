package com.quasar.fuego.operacion.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.quasar.fuego.operacion.service.LocalizacionService;

@SpringBootTest
@RunWith(BlockJUnit4ClassRunner.class)
public class LocalizacionServiceTest {

	@Autowired
	LocalizacionService localizacionService;

	@Test
	public void testLocalizacion() {
		double[][] posicion = new double[][] { { 2.0 }, { 3.0 }, { 4.0 } };
		double[] distances = new double[] { 2.2, 0.3, 1.1 };
		double[] posicionEsperada = new double[] { 4.202984918012354 };
		double[] posicionCalculada = localizacionService.getLocation(posicion, distances);
		for (int i = 0; i < posicionCalculada.length; i++) {
			assertEquals(posicionEsperada[i], posicionCalculada[i]);
		}

	}

}
