package com.quasar.fuego.operacion.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.quasar.fuego.operacion.config.SwaggerConfig;
import com.quasar.fuego.operacion.dto.ResponseShipDto;
import com.quasar.fuego.operacion.entities.Satellite;
import com.quasar.fuego.operacion.entities.SatelliteContainer;
import com.quasar.fuego.operacion.exceptions.MessageException;
import com.quasar.fuego.operacion.exceptions.SatelliteExceptions;
import com.quasar.fuego.operacion.service.AlianzaService;
import com.quasar.fuego.operacion.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "${context.path}")
@Api(tags = { SwaggerConfig.COMUNICATION })
public class ComunicacionRebeldeController {

	@Autowired
	private AlianzaService alianzaService;

	@Autowired
	private MessageService messageService;

	@ApiOperation(value = "Enpoint que al enviar un payload con los datos de los mensajes de los satelites, devuelve los mensajes decodificados", response = ResponseShipDto.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Mensajes decodificados con exito."),
			@ApiResponse(code = 404, message = "No fue posible decodificar el mensaje") })
	@PostMapping("/topsecret")
	public ResponseEntity<Object> topSecret(@RequestBody SatelliteContainer request) {
		Map<String, Object> response = new HashMap<>();
		ResponseShipDto dto;

		try {
			dto = alianzaService.getInfoFromShip(request);
		} catch (Exception e) {
			response.put("error: ", e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@ApiOperation(value = "Endpoint que recibe los datos de los satelites individualmente.", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "El mensaje fue guardado exitosamente."),
			@ApiResponse(code = 400, message = "Hay un problema en la requisicion.") })
	@PostMapping("topsecret_split/{satellite_name}")
	public ResponseEntity<Object> topSecretSplitPost(@RequestBody Satellite request,
			@PathVariable("satellite_name") String name) {
		Map<String, Object> response = new HashMap<>();

		if (request.getMessage() == null || request.getMessage().isEmpty()) {
			response.put("error: ", "Error en el request: Mensaje es indefinido.");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

		if (request.getDistance() == 0) {
			response.put("error: ", "Error en el request: Distancia esta indefinida.");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			request.setName(name);
			messageService.saveMessages(request);
			response.put("info: ", "El mensaje fue guardado exitosamente!");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (SatelliteExceptions e) {
			response.put("error: ", e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(value = "Obtiene los mensajes decodificados a partir de la lista de satelites guardados anteriormente", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Mensajes decodificados con exito."),
			@ApiResponse(code = 404, message = "No fue posible decodificar el mensaje") })
	/**
	 * Obtiene los mensajes decodificados a partir de la lista de satelites guardados anteriormente
	 * @return Mensajes decodificados
	 */
	@GetMapping("/topsecret_split")
	public ResponseEntity<Object> topSecretSplitGet() {
		Map<String, Object> response = new HashMap<>();
		ResponseShipDto dto;
		try {
			dto = alianzaService.getInfoFromShipSplit();
		} catch (MessageException e) {
			response.put("error: ", e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

}
