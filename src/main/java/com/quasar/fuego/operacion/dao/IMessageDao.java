package com.quasar.fuego.operacion.dao;

import com.quasar.fuego.operacion.entities.Satellite;
import com.quasar.fuego.operacion.entities.SatelliteContainer;


public interface IMessageDao {
	
	public void saveMessages(Satellite satelliteData);
	public SatelliteContainer recoveryMessagesFromSatellites();
	

}
