package com.quasar.fuego.operacion.service;

import com.quasar.fuego.operacion.dto.ResponseShipDto;
import com.quasar.fuego.operacion.entities.SatelliteContainer;
import com.quasar.fuego.operacion.exceptions.MessageException;

public interface AlianzaService {
	
	public ResponseShipDto getInfoFromShip(SatelliteContainer request) throws MessageException;
	public ResponseShipDto getInfoFromShipSplit() throws MessageException;

}
