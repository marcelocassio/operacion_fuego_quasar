package com.quasar.fuego.operacion.service.impl;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import com.quasar.fuego.operacion.dto.ResponseShipDto;
import com.quasar.fuego.operacion.entities.Position;
import com.quasar.fuego.operacion.entities.SatelliteContainer;
import com.quasar.fuego.operacion.exceptions.MessageException;
import com.quasar.fuego.operacion.service.AlianzaService;
import com.quasar.fuego.operacion.service.LocalizacionService;
import com.quasar.fuego.operacion.service.MessageService;

@Service
public class AlianzaServiceImpl implements AlianzaService {

	@Autowired
	private MessageService messageService;

	@Autowired
	private LocalizacionService localizacionService;

	@Autowired
	private Environment environment;

	@Override
	public ResponseShipDto getInfoFromShip(SatelliteContainer container) throws MessageException {
		ResponseShipDto response = new ResponseShipDto();
		if (container != null) {
			if (container.getMessages().size() < 3)
				throw new MessageException("Numero de satelites insuficientes");
		} else {
			throw new MessageException("Error al obtener datos de los satelites");
		}
		response.setMessage(messageService.getMessage(container.getMessages()));
		updatePositions(container);
		response.setPosition(
				new Position(localizacionService.getLocation(container.getPositions(), container.getDistances())));

		return response;
	}

	private void updatePositions(SatelliteContainer satelliteContainer) {

		int numberSatellites = Integer.parseInt(environment.getProperty("satellites.numbers"));

		String[] position;
		double[][] positions = new double[numberSatellites][];
		for (int i = 0; i < satelliteContainer.getSatellites().size(); i++) {
			position = environment.getProperty("satellite." + i + ".position").split(",");
			positions[i] = Arrays.stream(position).map(Double::valueOf).mapToDouble(Double::doubleValue).toArray();
		}
		satelliteContainer.setPositions(positions);
	}

	/**
	 * Devuelve el mensaje y la posicion de la nave 
	 */
	public ResponseShipDto getInfoFromShipSplit() throws MessageException {
		return getInfoFromShip(messageService.recoveryMessagesFromSatellites());

	}

}
