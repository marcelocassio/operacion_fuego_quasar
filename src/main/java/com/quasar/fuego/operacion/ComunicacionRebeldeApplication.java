package com.quasar.fuego.operacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComunicacionRebeldeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComunicacionRebeldeApplication.class, args);
	}

}
