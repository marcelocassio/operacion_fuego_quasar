package com.quasar.fuego.operacion.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.quasar.fuego.operacion.entities.Satellite;
import com.quasar.fuego.operacion.entities.SatelliteContainer;

@Repository
public class MessageDaoImpl implements IMessageDao {

	@Autowired
	private SatelliteContainer container;

	/**
	 * Guarda los datos del satelite en en contenedor de satelites
	 */
	@Override
	public void saveMessages(Satellite satelliteData) {
		container.getSatellites().add(satelliteData);
	}
	

	/**
	 * Devuelve el los datos de los satelites del contenedor
	 */
	@Override
	public SatelliteContainer recoveryMessagesFromSatellites() {
		return container;
	}

}
